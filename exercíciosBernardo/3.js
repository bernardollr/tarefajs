var n1 = parseFloat(prompt("Digite a primeira nota:"))
var n2 = parseFloat(prompt("Digite a segunda nota:"))
var n3 = parseFloat(prompt("Digite a terceira nota nota:"))

var média = (n1 + n2 + n3)/3
média = média.toFixed(2)
média = parseFloat(média)

if (média >= 6.0) {
    console.log("Parabéns, esse aluno passou com a média de " + média)
}
else {
    console.log("Que pena, esse aluno está reprovado por conta da média de " + média)
}