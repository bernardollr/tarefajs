function MultiMatriz() {
    let matrizA = [[],[]];
    let matrizB = [[],[]];
    let resultado = [[],[]];
    for (let i = 0; i < 2; i++) {
        for (let j = 0; j < 2; j++) {
            matrizA[i][j] = parseFloat(prompt("Digite o elemento ${i}, ${j} da primeira matriz"));
            matrizB[i][j] = parseFloat(prompt("Digite o elemento ${i}, ${j} da segunda matriz"));
            resultado[i][j] = matrizA[i][j] + matrizB[i][j];
        }
    }
    return resultado;
}

let matrizC = MultiMatriz();
console.log(matrizC);